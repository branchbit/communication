<?php

$autoloader = include __DIR__ . "/../vendor/autoload.php";

use BBit\Communication\Printers\Printer;
use BBit\Communication\Printers\Adapter\Dymo\ImageAdapter;

$file = realpath(__DIR__ . '/Resources/sticker.jpg');
$width = 960;
$height = 425;

$adapter = new ImageAdapter($height, $width);
$printStrategy = new \BBit\Communication\DataWriter\SocketStrategy('192.168.1.102', '9998');

$printer = new Printer($adapter, $printStrategy);

echo 'printing' . PHP_EOL;

$printer->execute($file, array());