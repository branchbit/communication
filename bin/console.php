#!/usr/bin/env php
<?php

set_time_limit(0);

// include the composer autoloader
require_once __DIR__ . '/../vendor/autoload.php';

// import the Symfony Console Application 
use Symfony\Component\Console\Application;

$app = new Application();
$app->add(new \BBit\Communication\Command\RegisterSocketListen());
$app->add(new \BBit\Communication\Command\PrinterUdpServer());
$app->add(new \BBit\Communication\Command\PrinterTcpServer());
$app->add(new \BBit\Communication\Command\PrinterSpool());
$app->run();