UDEV RULES
==========

in `/etc/udev/rules.d/01-first-udev.rules`:

```
ACTION=="add",SUBSYSTEM=="usbmisc", DRIVERS=="usb", ATTRS{idVendor}=="2730", ATTRS{idProduct}=="2002", SYMLINK:="ticketPrinter", MODE:="0777"
ACTION=="add",SUBSYSTEM=="usbmisc", DRIVERS=="usb", ATTRS{idVendor}=="0922", ATTRS{idProduct}=="0021", SYMLINK:="stickerPrinter", MODE:="0777"
ACTION=="add",SUBSYSTEM=="tty", DRIVERS=="usb", ATTRS{idVendor}=="067b", ATTRS{idProduct}=="2303", SYMLINK:="kassaOpener", MODE:="0777"
```


cronjobs:

```
* * * * * lockfile -r 0 /tmp/sock.register.lock && /home/pi/printer/bin/console.php bbit:register:socket 9991 /dev/kassaOpener -e && rm -f /tmp/sock.register.lock
* * * * * lockfile -r 0 /tmp/sock.prnt.ticket.lock && /home/pi/printer/bin/console.php bbit:printer:tcp 9989 /dev/ticketPrinter -c && rm -f /tmp/sock.prnt.ticket.lock
* * * * * lockfile -r 0 /tmp/sock.prnt.sticker.lock && /home/pi/printer/bin/console.php bbit:printer:tcp 9999 /tmp/stickers/ -s && rm -f /tmp/sock.prnt.sticker.lock
* * * * * lockfile -r 0 /tmp/sock.prnt.sticker_spool.lock && /home/pi/printer/bin/console.php bbit:printer:spool /tmp/stickers/ /dev/stickerPrinter -f && rm -f /tmp/sock.prnt.sticker_spool.lock
```