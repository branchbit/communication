<?php

namespace BBit\Communication\DataWriter;

abstract class AbstractStrategy
{
    abstract public function execute($content, $options = array());
}