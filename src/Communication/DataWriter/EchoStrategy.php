<?php

namespace BBit\Communication\DataWriter;

class EchoStrategy extends AbstractDeviceStrategy
{
    /**
     * @param $content
     * @param array $options
     */
    public function execute($content, $options = array())
    {
        $content = escapeshellarg($content);
        system("echo $content > {$this->device}");
    }
}