<?php

namespace BBit\Communication\DataWriter;

class FileStrategy extends AbstractDeviceStrategy
{
    /**
     * @param $content
     * @param array $options
     */
    public function execute($content, $options = array())
    {
        file_put_contents($this->device, $content);
    }
}