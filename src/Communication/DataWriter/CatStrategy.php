<?php

namespace BBit\Communication\DataWriter;

class CatStrategy extends AbstractDeviceStrategy
{
    protected function createFilename()
    {
        return '/tmp/' . uniqid('ticket_');
    }

    /**
     * @param $content
     * @param array $options
     */
    public function execute($content, $options = array())
    {
        $filename = $this->createFilename();
        file_put_contents($filename, $content);
        system("cat $filename > /dev/ticketPrinter");
        unlink($filename);
    }
}
