<?php

namespace BBit\Communication\DataWriter;

class SocketStrategy extends AbstractStrategy
{
    /**
     * @var string|null
     */
    protected $ip;

    /**
     * @var string|int|null
     */
    protected $port;

    public function __construct($ip = null, $port = null)
    {
        $this->ip = $ip;
        $this->port = $port;
    }

    /**
     * @return null|string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param null|string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return int|null|string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int|null|string $port
     * @return $this
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return resource
     */
    protected function createSocket()
    {
        if (!($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP))) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);

            throw new \RuntimeException("Couldn't create socket: [$errorcode] $errormsg \n");
        }

        return $sock;
    }

    /**
     * @param $content
     * @param array $options
     */
    public function execute($content, $options = array())
    {
        $ip     = isset($options['ip']) ? $options['ip']: $this->ip;
        $port   = isset($options['port']) ? $options['port']: $this->ip;
        $sock   = $this->createSocket();

        file_put_contents('/tmp/printer_sockets.log', "sending data to socket: $ip:$port\n", FILE_APPEND);

        if(isset($options['prefix'])) {
            $content = "PREFIX:{$options['prefix']}\n$content";
        }

        $connected = socket_connect($sock, $ip, (int) $port);

        if(!$connected)
            throw new \RuntimeException("Could not connect to $ip:$port");

        $sentBytes = socket_write($sock, $content, strlen($content) + 32);

        if ($sentBytes === false) {
            file_put_contents('/tmp/printer_sockets.log', "sending data failed\n", FILE_APPEND);
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            file_put_contents('/tmp/printer_sockets.log', "Couldn't create socket: [$errorcode] $errormsg \n", FILE_APPEND);
        } else {
            file_put_contents('/tmp/printer_sockets.log', "sent $sentBytes\n", FILE_APPEND);
        }

        socket_close($sock);
    }
}