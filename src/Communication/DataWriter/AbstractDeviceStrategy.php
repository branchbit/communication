<?php

namespace BBit\Communication\DataWriter;

abstract class AbstractDeviceStrategy
{
    protected $device;

    public function __construct($device)
    {
        $this->device = $device;
    }
}