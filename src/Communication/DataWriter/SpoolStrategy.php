<?php

namespace BBit\Communication\DataWriter;

class SpoolStrategy extends AbstractStrategy
{
    /**
     * @var String
     */
    protected $folder;

    public function __construct($folder)
    {
        if(!is_dir($folder))
            mkdir($folder);

        $this->folder = $folder;
    }

    public function execute($content, $options = array())
    {
        if(isset($options['prefix'])){
            $prefix = $options['prefix'] . '_';
        }else{
            $prefix = 'xxx_';
        }

        $file = $this->folder . $prefix . time() .'_'.md5($content).'.tmp';
        file_put_contents($file,$content);
        rename($file, str_replace('.tmp', '.ready', $file));
    }
}