<?php

namespace BBit\Communication\Printers;

use BBit\Communication\Printers\Adapter\AbstractAdapter;
use BBit\Communication\DataWriter\AbstractStrategy;

class Printer
{
    /**
     * @var AbstractAdapter
     */
    protected $adapter;

    /**
     * @var AbstractStrategy
     */
    protected $printerStrategy;

    /**
     * @var bool
     */
    protected $debug;

    public function __construct(AbstractAdapter $adapter, AbstractStrategy $printerStrategy)
    {
        $this->adapter = $adapter;
        $this->printerStrategy = $printerStrategy;
    }

    public function execute($data, $options = array())
    {
        if ($this->debug) {
             return;
        }

        $converted = $this->adapter->convert($data, $options);

        if ($converted) {
            $this->printerStrategy->execute($converted, $options);
        }
    }

    /**
     * @return boolean
     */
    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @param boolean $debug
     * @return $this
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }
}
