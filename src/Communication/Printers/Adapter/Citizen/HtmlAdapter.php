<?php

namespace BBit\Communication\Printers\Adapter\Citizen;

use BBit\Communication\Printers\Adapter as PrinterAdapters;

class HtmlAdapter extends PrinterAdapters\AbstractAdapter
{
    public function convert($data, $options = array())
    {
        return str_replace("'", "'\''", $data);
    }
}