<?php

namespace BBit\Communication\Printers\Adapter;

abstract class AbstractAdapter
{
    abstract public function convert($data, $options = array());
}