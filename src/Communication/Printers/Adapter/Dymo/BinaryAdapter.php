<?php

namespace BBit\Communication\Printers\Adapter\Dymo;

use BBit\Communication\Printers\Adapter as PrinterAdapters;

define('__PRINTER_DYMO_DYNAMIC_ESCAPE', str_repeat("\x1B", 84));

class BinaryAdapter extends PrinterAdapters\AbstractAdapter
{
    // commands
    const CMD_NEWLINE           = "\x16";
    const CMD_NEWLINE_SIZE      = "\x1B\x44";
    const CMD_FEED              = "\x1B\x45";
    const CMD_RESET             = __PRINTER_DYMO_DYNAMIC_ESCAPE;

    // colors
    const COLOR_BLACK   = "1";
    const COLOR_WHITE   = "0";

    /**
     * @var int
     */
    protected $lineWidth;

    /**
     * @var int
     */
    protected $maxLines;

    /**
     * @var string[]
     */
    protected $lines = array();

    protected $currentLine;

    protected $currentLineByte = "";

    public function __construct($lineWidth, $maxLines)
    {
        $this->lineWidth = $lineWidth;
        $this->maxLines = $maxLines;
    }

    public function getLineWidth()
    {
        return $this->lineWidth;
    }

    public function getMaxLines()
    {
        return $this->maxLines;
    }

    public function createLine()
    {
        $this->currentLine = "";
        $this->currentLineByte = "";
    }

    public function endLine()
    {
        if (strlen($this->currentLineByte)) {
            $this->currentLine .= chr(bindec(str_pad($this->currentLineByte, 8, '0', STR_PAD_RIGHT)));
        }

        if (count($this->lines) <= $this->maxLines) {
            array_unshift($this->lines, $this->currentLine);
            return $this;
        }

        throw new \RuntimeException('too many lines');
    }

    public function addPixel($color)
    {
        $this->currentLineByte .= ($color > (16777215/2) ? self::COLOR_WHITE: self::COLOR_BLACK);
        if (strlen($this->currentLineByte) == 8) {
            $this->currentLine .= chr(bindec($this->currentLineByte));
            $this->currentLineByte = "";
        }
    }

    public function feed()
    {
        return self::CMD_FEED;
    }

    public function reset()
    {
        return self::CMD_RESET;
    }

    public function convert($data, $options = array())
    {
        if (!count($this->lines)) {
            return "";
        }

        $binary = self::CMD_RESET;
        foreach ($this->lines as $line) {
            $binary .= self::CMD_NEWLINE_SIZE . chr(strlen($line)) . self::CMD_NEWLINE .  $line;
        }

        $binary .= self::CMD_FEED;

        return $binary;
    }
}