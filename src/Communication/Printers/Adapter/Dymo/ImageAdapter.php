<?php

namespace BBit\Communication\Printers\Adapter\Dymo;

class ImageAdapter extends BinaryAdapter
{
    public function convert($data, $options = array())
    {
        $this->readImage($data);

        return parent::convert($data, $options);
    }

    public function readImage($file)
    {
        // clear lines
        $this->lines = array();

        // read image file
        $sticker = @imagecreatefromjpeg($file);
        if (!$sticker) throw new \RuntimeException("Cannot Initialize new GD image stream: " . $file);

        // loop pixels
        $x = 0;
        while ($x < $this->maxLines) {
            $y = 0;
            $this->createLine();

            while ($y < $this->lineWidth) {
                $this->addPixel(imagecolorat($sticker, $x, $y));
                $y++;
            }

            $this->endLine();
            $x++;
        }
    }
}