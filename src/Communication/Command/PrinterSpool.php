<?php
namespace BBit\Communication\Command;

use BBit\Communication\DataWriter\CatStrategy;
use BBit\Communication\DataWriter\EchoStrategy;
use BBit\Communication\DataWriter\FileStrategy;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PrinterSpool extends Command
{
    protected $logfile = '/tmp/printer_spool.log';

    protected function configure()
    {
        $this
            ->addArgument(
                'folder',
                InputArgument::REQUIRED,
                'What port should I listen on?'
            )
            ->addArgument(
                'device',
                InputArgument::REQUIRED,
                'What device should I print to?'
            )
            ->addOption(
                'cat', 'c',
                InputOption::VALUE_NONE,
                'use the CatStrategy to write data (ticket printer)'
            )
            ->addOption(
                'file', 'f',
                InputOption::VALUE_NONE,
                'use the FileStrategy to write data (sticker printer)'
            )
            ->addOption(
                'echo', 'e',
                InputOption::VALUE_NONE,
                'use the EchoStrategy to write data (register)'
            )
        ;

        $this
            ->setName('bbit:printer:spool')
            ->setDescription('Prints stickers.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $strategy = $this->createStrategy($input);

        while(1){

            if(!is_dir($dir = $input->getArgument('folder')))
                mkdir($dir);


            $files = glob($dir . "*.ready");

            if(!$files || !is_array($files)){
                continue;
            }

            $file = $files[0];

            $filename = basename($file);
            $id       = explode('_', $filename);
            $id       = $id[0];

            $orderStickers = glob($dir . $id.'_*.ready');

            while($orderStickers){
                $output->writeln($orderStickers[0]);
                $strategy->execute(file_get_contents($orderStickers[0]));
                unlink($orderStickers[0]);
                $orderStickers = glob($dir . $id.'_*.ready');
            }
        }
    }

    /**
     * @param InputInterface $input
     * @return CatStrategy|EchoStrategy|FileStrategy
     */
    protected function createStrategy(InputInterface $input)
    {
        if ($input->getOption('file')) {
            return new FileStrategy($input->getArgument('device'));
        }
        if ($input->getOption('cat')) {
            return new CatStrategy($input->getArgument('device'));
        }
        if ($input->getOption('echo')) {
            return new EchoStrategy($input->getArgument('device'));
        }

        throw new \InvalidArgumentException('no valid strategy selected');
    }

}