<?php

namespace BBit\Communication\Command;

use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class WriteDataReact extends Command
{
    protected $logFile;

    protected function configure()
    {
        $this
            ->setName('bbit:write:react')
            ->setDescription('write data to a server using react')
            ->addOption('port', null, InputOption::VALUE_REQUIRED)
            ->addOption('ip', null, InputOption::VALUE_REQUIRED)
            ->addArgument('data', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $loop = \React\EventLoop\Factory::create();

        $client = stream_socket_client('tcp://'. $input->getOption('ip').':'. $input->getOption('port'));

        $conn = new \React\Stream\Stream($client, $loop);

        /** @var DialogHelper $dialog */
        $dialog = $this->getHelper('dialog');
        $conn->on('data', function ($data) use ($conn, $output, $dialog) {
            if (!$data) return;

            $output->write($data);

            $data = $dialog->ask($output, '> ');
            $conn->write($data. "\n");
        });

        $loop->run();

        $data = $input->getArgument('data');
        if (!$data) {
            $data = $dialog->ask($output, '> ');
        }

        $conn->write($data. "\n");
    }
}