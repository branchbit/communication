<?php

namespace BBit\Communication\Command;


class PrinterTcpServer extends AbstractTcpServer
{
    protected $logFile = '/tmp/printer_tcp.log';

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('bbit:printer:tcp')
            ->setDescription('create a react server for incoming data and send everything to a printer')
        ;
    }
}