<?php

namespace BBit\Communication\Command;

class PrinterReactServer extends AbstractReactServer
{
    protected $logFile = '/tmp/printer_react.log';

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('bbit:printer:react')
            ->setDescription('create a react server for incoming data and send everything to a printer')
        ;
    }
}