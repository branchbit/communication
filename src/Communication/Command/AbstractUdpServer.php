<?php

namespace BBit\Communication\Command;

use BBit\Communication\DataWriter\CatStrategy;
use BBit\Communication\DataWriter\EchoStrategy;
use BBit\Communication\DataWriter\FileStrategy;
use BBit\Communication\Socket\SocketListener;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AbstractUdpServer extends Command
{
    protected $logFile;

    protected function configure()
    {
        $this
            ->setName('bbit:printer:socket')
            ->setDescription('create a socket for incoming data and send everything to a writer strategy')
            ->addArgument(
                'port',
                InputArgument::REQUIRED,
                'What port should I listen on?'
            )
            ->addArgument(
                'device',
                InputArgument::REQUIRED,
                'What device should I print to?'
            )
            ->addOption(
                'ip', null,
                InputOption::VALUE_OPTIONAL,
                'If set, only incoming data from this ip is allowed',
                SocketListener::LISTEN_IP_ALL
            )
            ->addOption(
                'cat', 'c',
                InputOption::VALUE_NONE,
                'use the CatStrategy to write data (ticket printer)'
            )
            ->addOption(
                'file', 'f',
                InputOption::VALUE_NONE,
                'use the FileStrategy to write data (sticker printer)'
            )
            ->addOption(
                'echo', 'e',
                InputOption::VALUE_NONE,
                'use the EchoStrategy to write data (register)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $socket = new SocketListener($input->getOption('ip'), $input->getArgument('port'));

        // create a processing strategy
        $strategy = $this->createStrategy($input);

        // loop the listening
        while(1)
        {
            file_put_contents($this->logFile, "listening to socket: {$input->getOption('ip')}:{$input->getArgument('port')}\n", FILE_APPEND);

            // blocking function until socket receives data
            $content = $socket->listen();

            // data received
            file_put_contents($this->logFile, "data received:\n", FILE_APPEND);
            file_put_contents($this->logFile, "$content\n", FILE_APPEND);

            // data received > execute
            $strategy->execute($content);
        }

        $socket->close();
    }

    protected function createStrategy(InputInterface $input)
    {
        if ($input->getOption('file')) {
            return new FileStrategy($input->getArgument('device'));
        }
        if ($input->getOption('cat')) {
            return new CatStrategy($input->getArgument('device'));
        }
        if ($input->getOption('echo')) {
            return new EchoStrategy($input->getArgument('device'));
        }

        throw new \InvalidArgumentException('no valid strategy selected');
    }
}