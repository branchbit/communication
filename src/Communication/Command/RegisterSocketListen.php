<?php

namespace BBit\Communication\Command;

use BBit\Communication\DataWriter\CatStrategy;
use BBit\Communication\DataWriter\FileStrategy;
use BBit\Communication\Socket\SocketListener;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RegisterSocketListen extends AbstractTcpServer
{
    protected $logFile = '/tmp/register_sockets.log';

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('bbit:register:socket')
            ->setDescription('create a socket for incoming message and open the register')
        ;
    }
}