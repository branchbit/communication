<?php

namespace BBit\Communication\Command;

use BBit\Communication\DataWriter\CatStrategy;
use BBit\Communication\DataWriter\EchoStrategy;
use BBit\Communication\DataWriter\FileStrategy;
use BBit\Communication\Socket\SocketListener;
use React\Socket\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AbstractReactServer extends Command
{
    protected $logFile;

    protected function configure()
    {
        $this
            ->addArgument(
                'port',
                InputArgument::REQUIRED,
                'What port should I listen on?'
            )
            ->addArgument(
                'device',
                InputArgument::REQUIRED,
                'What device should I print to?'
            )
            ->addOption(
                'ip', null,
                InputOption::VALUE_OPTIONAL,
                'If set, only incoming data from this ip is allowed',
                SocketListener::LISTEN_IP_ALL
            )
            ->addOption(
                'cat', 'c',
                InputOption::VALUE_NONE,
                'use the CatStrategy to write data (ticket printer)'
            )
            ->addOption(
                'file', 'f',
                InputOption::VALUE_NONE,
                'use the FileStrategy to write data (sticker printer)'
            )
            ->addOption(
                'echo', 'e',
                InputOption::VALUE_NONE,
                'use the EchoStrategy to write data (register)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logFile = $this->logFile;

        // create a processing strategy
        $strategy = $this->createStrategy($input);

        $loop = \React\EventLoop\Factory::create();

        $socket = new \React\Socket\Server($loop);
        $socket->on('connection', function ($conn) use ($strategy, $logFile) {

            /** @var $conn Connection */

            file_put_contents($logFile, "connected\n", FILE_APPEND);

            $conn->write("CONNECTED\n");
            $conn->on('data', function ($data, $conn) use ($strategy, $logFile) {


                /** @var $conn Connection */
                $conn->write("DATA RECEIVED\n");

                // data received
                file_put_contents($logFile, "data received:\n", FILE_APPEND);
                file_put_contents($logFile, "$data\n", FILE_APPEND);

                $newline = strpos($data, "\n") + 1;
                $firstLine = substr($data, 0, $newline);

                if (strpos($firstLine, 'HASH:') !== false) {
                    $data = substr($data, $newline + 1);

                    $hash = substr($firstLine, 5);
                    if (md5($data) !== $hash) {
                        file_put_contents($logFile, "HASH MISMATCH\n", FILE_APPEND);
                        $conn->write("HASH MISMATCH\n");
                        return;
                    }
                    $conn->write("HASH OK\n");
                }

                // data received > execute
                $strategy->execute($data);

                file_put_contents($logFile, "data processed\n", FILE_APPEND);

                $conn->write("READY\n");
            });
        });

        $socket->listen($input->getArgument('port'), $input->getOption('ip'));
        file_put_contents($this->logFile, "listening to socket: {$input->getOption('ip')}:{$input->getArgument('port')}\n", FILE_APPEND);

        $loop->run();
    }

    /**
     * @param InputInterface $input
     * @return CatStrategy|EchoStrategy|FileStrategy
     */
    protected function createStrategy(InputInterface $input)
    {
        if ($input->getOption('file')) {
            return new FileStrategy($input->getArgument('device'));
        }
        if ($input->getOption('cat')) {
            return new CatStrategy($input->getArgument('device'));
        }
        if ($input->getOption('echo')) {
            return new EchoStrategy($input->getArgument('device'));
        }

        throw new \InvalidArgumentException('no valid strategy selected');
    }
}