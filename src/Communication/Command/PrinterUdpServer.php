<?php

namespace BBit\Communication\Command;

class PrinterUdpServer extends AbstractUdpServer
{
    protected $logFile = '/tmp/printer_sockets.log';

    protected function configure()
    {
        parent::configure();

        $this
            ->setName('bbit:printer:socket')
            ->setDescription('create a socket for incoming data and send everything to a printer')
        ;
    }
}