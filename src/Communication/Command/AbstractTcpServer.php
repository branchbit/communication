<?php
namespace BBit\Communication\Command;

use BBit\Communication\DataWriter\CatStrategy;
use BBit\Communication\DataWriter\EchoStrategy;
use BBit\Communication\DataWriter\FileStrategy;
use BBit\Communication\DataWriter\SpoolStrategy;
use BBit\Communication\Socket\SocketListener;
use BBit\Communication\Socket\TcpSocketListener;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AbstractTcpServer extends Command
{
    protected $logFile;

    protected function configure()
    {
        $this
            ->addArgument(
                'port',
                InputArgument::REQUIRED,
                'What port should I listen on?'
            )
            ->addArgument(
                'device',
                InputArgument::REQUIRED,
                'What device should I print to?'
            )
            ->addOption(
                'ip', null,
                InputOption::VALUE_OPTIONAL,
                'If set, only incoming data from this ip is allowed',
                SocketListener::LISTEN_IP_ALL
            )
            ->addOption(
                'cat', 'c',
                InputOption::VALUE_NONE,
                'use the CatStrategy to write data (ticket printer)'
            )
            ->addOption(
                'file', 'f',
                InputOption::VALUE_NONE,
                'use the FileStrategy to write data (sticker printer)'
            )
            ->addOption(
                'echo', 'e',
                InputOption::VALUE_NONE,
                'use the EchoStrategy to write data (register)'
            )
            ->addOption(
                'spool', 's',
                InputOption::VALUE_NONE,
                'use the EchoStrategy to write data (register)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logFile = $this->logFile;

        // create a processing strategy
        $strategy = $this->createStrategy($input);

        $socket = new TcpSocketListener($input->getOption('ip'), $input->getArgument('port'));

        // loop the listening
        while(1)
        {
            if(!($content = $socket->listen())){
                continue;
            }

            file_put_contents($logFile, "listening to socket: {$input->getOption('ip')}:{$input->getArgument('port')}\n", FILE_APPEND);
            // data received
            file_put_contents($logFile, "data received:\n", FILE_APPEND);
            file_put_contents($logFile, "$content\n", FILE_APPEND);

            try{
                $output->write("DATA RECEIVED\n");

                // data received
                file_put_contents($logFile, "data received:\n", FILE_APPEND);
                file_put_contents($logFile, "$content\n", FILE_APPEND);

                $newline    = strpos($content, "\n") + 1;
                $firstLine  = substr($content, 0, $newline);

                if (strpos($firstLine, 'PREFIX:') !== false) {
                    $content = substr($content, $newline + 1);
                    $prefix  = rtrim(substr($firstLine, 7),"\n");

                    // data received > execute
                    $strategy->execute($content, array('prefix' => $prefix));
                }elseif(strpos($firstLine, 'HASH:') !== false){
                    $content = substr($content, $newline + 1);
                    $prefix  = rtrim(substr($firstLine, 5),"\n");

                    // data received > execute
                    $strategy->execute($content, array('prefix' => $prefix));
                }else{
                    $strategy->execute($content);
                }

                file_put_contents($logFile, "data processed\n", FILE_APPEND);

                $output->write("READY\n");

            }catch(\Exception $e){
                $msg = $e->getMessage();
                file_put_contents($logFile, "ERROR : \n", FILE_APPEND);
                file_put_contents($logFile, "$msg\n", FILE_APPEND);
                echo $msg, PHP_EOL;
                //$socket->close();
                //die();
            }
        }

        $socket->close();
    }

    /**
     * @param InputInterface $input
     * @return CatStrategy|EchoStrategy|FileStrategy|SpoolStrategy
     */
    protected function createStrategy(InputInterface $input)
    {
        if ($input->getOption('file')) {
            return new FileStrategy($input->getArgument('device'));
        }
        if ($input->getOption('cat')) {
            return new CatStrategy($input->getArgument('device'));
        }
        if ($input->getOption('echo')) {
            return new EchoStrategy($input->getArgument('device'));
        }
        if ($input->getOption('spool')) {
            return new SpoolStrategy($input->getArgument('device'));
        }

        throw new \InvalidArgumentException('no valid strategy selected');
    }
}