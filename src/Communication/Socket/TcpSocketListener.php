<?php
namespace BBit\Communication\Socket;

class TcpSocketListener
{
    const LISTEN_ALL_IP = '0.0.0.0';

    /**
     * @var String
     */
    protected $ipAddress;

    /**
     * @var Integer
     */
    protected $port;

    /**
     * @var Resource
     */
    protected $socket;

    function __construct($ip, $port)
    {
        $this->ipAddress    = (string)  $ip;
        $this->port         = (int)     $port;
    }

    public function listen()
    {
        if($this->socket === NULL)
            $this->createSocket();

        return $this->waitForContent();
    }

    /**
     *  Creates a TCP Socket
     */
    protected function createSocket()
    {
        if(($socket = socket_create(AF_INET, SOCK_STREAM, 0)) < 0)
            throw new \RuntimeException('failed to create socket: '.socket_strerror($socket));


        if(($bind = socket_bind($socket, $this->ipAddress, $this->port)) < 0)
            throw new \RuntimeException('failed to bind socket:'.socket_strerror($bind));


        if(($list = socket_listen($socket, 0)) < 0 )
            throw new \RuntimeException('failed to listen to socket: '.socket_strerror($list));

        socket_set_nonblock($socket);

        $this->socket = $socket;
    }

    /**
     * @param null $remoteIp
     * @param null $remotePort
     *
     * @return String|Bool
     */
    protected function waitForContent(&$remoteIp = null, &$remotePort = null)
    {
        if($connection = @socket_accept($this->socket)){

            $bytesReceived = socket_recv($connection, $buffer, 1024 * 1024 * 150, 0);

            if ($bytesReceived === false)
                return false;

            return $buffer;
        }

        return false;

    }

    public function close()
    {
        if ($this->socket) {
            socket_close($this->socket);
            $this->socket = null;
        }
    }
}