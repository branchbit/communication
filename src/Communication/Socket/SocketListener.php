<?php

namespace BBit\Communication\Socket;

class SocketListener 
{
    const LISTEN_IP_ALL = '0.0.0.0';

    protected $ip;

    protected $port;

    protected $socket;

    protected $sendResponse = false;

    public function __construct($ip, $port)
    {
        $this->ip = $ip;
        $this->port = $port;
    }

    public function listen()
    {
        if (!$this->socket) {
            $this->createSocket();
        }

        $content = $this->waitForContent();
        if ($content === false) {
            return false;
        }

        return $content;
    }

    public function close()
    {
        if ($this->socket) {
            socket_close($this->socket);
            $this->socket = null;
        }
    }

    protected function createSocket()
    {
        // create a UDP socket
        if(!($sock = socket_create(AF_INET, SOCK_DGRAM, 0)))
        {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);

            throw new \RuntimeException("Couldn't create socket: [$errorcode] $errormsg \n");
        }

        // bind the source address
        if(!socket_bind($sock, $this->ip , $this->port)) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);

            throw new \RuntimeException("Could not bind socket : [$errorcode] $errormsg \n");
        }

        $this->socket = $sock;
    }

    protected function waitForContent(&$remoteIp = null, &$remotePort = null)
    {
        // function hangs until received
        $bytesReceived = socket_recvfrom($this->socket, $buffer, 1024 * 1024, 0, $remoteIp, $remotePort);

        if ($bytesReceived === false) return false;

        return $buffer;
    }

    protected function sendResponse($remoteIp, $remotePort)
    {
        socket_sendto($this->socket, "OK", 100, 0, $remoteIp, $remotePort);
    }
}